package cn.amos.iot.gateway;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TcpServerVerticle extends AbstractVerticle {

  @Override
  public void start(Future<Void> startFuture) throws Exception {
    vertx.createHttpServer().requestHandler(req -> {
      log.info("some visit 8899");
      req.response()
        .putHeader("content-type", "text/plain")
        .end("Hello from Vert.x!");
    }).listen(8899, http -> {
      if (http.succeeded()) {
        startFuture.complete();
        log.info("HTTP server started on port 8899");
      } else {
        startFuture.fail(http.cause());
      }
    });
  }
}
